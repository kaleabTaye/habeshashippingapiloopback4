import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Order extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  item?: string;

  @property({
    type: 'number',
  })
  numOfItems?: number;

  @property({
    type: 'number',
  })
  vechilesNeeded?: number;

  @property({
    type: 'boolean',
  })
  inProgress?: boolean;

  @property({
    type: 'boolean',
  })
  itemArrived?: boolean;


  constructor(data?: Partial<Order>) {
    super(data);
  }
}

export interface OrderRelations {
  // describe navigational properties here
}

export type OrderWithRelations = Order & OrderRelations;
