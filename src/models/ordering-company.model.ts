import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class OrderingCompany extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  companyName?: string;

  @property({
    type: 'string',
  })
  userName?: string;

  @property({
    type: 'string',
  })
  password?: string;


  constructor(data?: Partial<OrderingCompany>) {
    super(data);
  }
}

export interface OrderingCompanyRelations {
  // describe navigational properties here
}

export type OrderingCompanyWithRelations = OrderingCompany & OrderingCompanyRelations;
