import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Driver extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  userName?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'number',
  })
  experience?: number;

  @property({
    type: 'boolean',
  })
  isAvailable?: boolean;


  constructor(data?: Partial<Driver>) {
    super(data);
  }
}

export interface DriverRelations {
  // describe navigational properties here
}

export type DriverWithRelations = Driver & DriverRelations;
