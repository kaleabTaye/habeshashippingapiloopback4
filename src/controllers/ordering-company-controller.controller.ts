import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {OrderingCompany} from '../models';
import {OrderingCompanyRepository} from '../repositories';

export class OrderingCompanyControllerController {
  constructor(
    @repository(OrderingCompanyRepository)
    public orderingCompanyRepository : OrderingCompanyRepository,
  ) {}

  @post('/ordering-companies', {
    responses: {
      '200': {
        description: 'OrderingCompany model instance',
        content: {'application/json': {schema: {'x-ts-type': OrderingCompany}}},
      },
    },
  })
  async create(@requestBody() orderingCompany: OrderingCompany): Promise<OrderingCompany> {
    return await this.orderingCompanyRepository.create(orderingCompany);
  }

  @get('/ordering-companies/count', {
    responses: {
      '200': {
        description: 'OrderingCompany model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(OrderingCompany)) where?: Where<OrderingCompany>,
  ): Promise<Count> {
    return await this.orderingCompanyRepository.count(where);
  }

  @get('/ordering-companies', {
    responses: {
      '200': {
        description: 'Array of OrderingCompany model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': OrderingCompany}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(OrderingCompany)) filter?: Filter<OrderingCompany>,
  ): Promise<OrderingCompany[]> {
    return await this.orderingCompanyRepository.find(filter);
  }

  @patch('/ordering-companies', {
    responses: {
      '200': {
        description: 'OrderingCompany PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() orderingCompany: OrderingCompany,
    @param.query.object('where', getWhereSchemaFor(OrderingCompany)) where?: Where<OrderingCompany>,
  ): Promise<Count> {
    return await this.orderingCompanyRepository.updateAll(orderingCompany, where);
  }

  @get('/ordering-companies/{id}', {
    responses: {
      '200': {
        description: 'OrderingCompany model instance',
        content: {'application/json': {schema: {'x-ts-type': OrderingCompany}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<OrderingCompany> {
    return await this.orderingCompanyRepository.findById(id);
  }

  @patch('/ordering-companies/{id}', {
    responses: {
      '204': {
        description: 'OrderingCompany PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() orderingCompany: OrderingCompany,
  ): Promise<void> {
    await this.orderingCompanyRepository.updateById(id, orderingCompany);
  }

  @put('/ordering-companies/{id}', {
    responses: {
      '204': {
        description: 'OrderingCompany PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() orderingCompany: OrderingCompany,
  ): Promise<void> {
    await this.orderingCompanyRepository.replaceById(id, orderingCompany);
  }

  @del('/ordering-companies/{id}', {
    responses: {
      '204': {
        description: 'OrderingCompany DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderingCompanyRepository.deleteById(id);
  }
}
