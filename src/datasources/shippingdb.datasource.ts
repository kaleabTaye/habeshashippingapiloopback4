import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './shippingdb.datasource.json';

export class ShippingdbDataSource extends juggler.DataSource {
  static dataSourceName = 'shippingdb';

  constructor(
    @inject('datasources.config.shippingdb', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
