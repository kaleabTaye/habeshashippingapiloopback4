import {DefaultCrudRepository} from '@loopback/repository';
import {Driver, DriverRelations} from '../models';
import {ShippingdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DriverRepository extends DefaultCrudRepository<
  Driver,
  typeof Driver.prototype.id,
  DriverRelations
> {
  constructor(
    @inject('datasources.shippingdb') dataSource: ShippingdbDataSource,
  ) {
    super(Driver, dataSource);
  }
}
