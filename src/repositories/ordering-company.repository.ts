import {DefaultCrudRepository} from '@loopback/repository';
import {OrderingCompany, OrderingCompanyRelations} from '../models';
import {ShippingdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OrderingCompanyRepository extends DefaultCrudRepository<
  OrderingCompany,
  typeof OrderingCompany.prototype.id,
  OrderingCompanyRelations
> {
  constructor(
    @inject('datasources.shippingdb') dataSource: ShippingdbDataSource,
  ) {
    super(OrderingCompany, dataSource);
  }
}
